

/******************************************************************************************************************************************/
/************************************************** IDENTIFICACAO DO VENCIMENTO CORRETO ***************************************************/
/******************************************************************************************************************************************/


create temporary table tab_financ_parcela3 as
select 	fpp.emprestimo_id,
		fpp.status as status_fpp,
		fp.plano_id,
		fp.id,
		fp.numero,
		lpad(fp.numero||''||lpad(cast(fpp.emprestimo_id as varchar),15,'0'),17,'0') as chave,
		fp.cobrado,	
		--fp.juros,
		fp.pago,
		fp.vencimento_em,
		fp.pago_em,
		fp.status
from financeiro_parcela fp
left join financeiro_planopagamento fpp on fpp.id = fp.plano_id
--left join installment_actual2 i on i.installment_id = fp.id
--where plano_id in ('15', '223', '264', '696')
order by fpp.emprestimo_id, fp.plano_id, numero;



create temporary table equivale_installment_actual2 as
select  fp.id as installment_id,
			 fp.cobrado as value,
			 vencimento_em as due_date,
			 pago_em as date_paid	,
			 fp.numero as installment_number,
			 fp.pago as amount_paid,
			 fp.juros as interest, 
			 emprestimo_id as loan_request_id
from financeiro_planopagamento fpp
join financeiro_parcela fp on fp.plano_id = fpp.id
where fp.status not in  ('Cancelada', 'EmEspera') /*and emprestimo_id = '60'*/
order by emprestimo_id, fp.id,  numero;


/******************************************* CRIA CHAVE A PARTIR DA "MINHA"  installment_actual2 ***************************************************/
create temporary table tab_installm_act2 as
select  fpp.emprestimo_id,	
		fp.plano_id,
		fpp.atraso_max_dias as atraso_max_dias_fpp,
		i.installment_id,
		i.installment_number,
		lpad(i.installment_number||''||lpad(cast(fpp.emprestimo_id as varchar),15,'0'),17,'0') as chave,
		i.value,
		i.amount_paid,
		i.interest as juros,
		i.due_date,
		i.date_paid				
from equivale_installment_actual2 i
join financeiro_parcela fp on fp.id = i.installment_id
join financeiro_planopagamento fpp on fpp.id = fp.plano_id
--join loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
--where plano_id in ('15', '223', '264', '696')
order by fpp.emprestimo_id, fp.plano_id, installment_number;



create temporary table menor_parc4 as
	 select plano_id,
/*3*/		min(menor_parcela) as menor_parcela
	  from (
--Cruzamento entre as duas tabelas de cima: financeiro_parcela e installment_actual2 - (III)	
	  select tab_fp.emprestimo_id,
			tab_fp.plano_id,
			id,
			installment_id,
			tab_fp.chave,
			tab_i.chave,
			numero,
			installment_number,
			atraso_max_dias_fpp,
			cobrado,
			juros,
			value,
			status_fpp,
			vencimento_em,
			due_date,
/*2*/		case when cobrado <> value then installment_number else null end as menor_parcela
	from tab_financ_parcela3 tab_fp
	left join tab_installm_act2 tab_i on tab_fp.chave =tab_i.chave
	order by tab_fp.emprestimo_id, plano_id, installment_number) as t5
	group by 1
	order by plano_id;

	
	
-- Pega o max(marca_plano) para depois identificar se o plano foi ativo em algum momento, ou seja, nao tem soh status "Cancelada"	
create temporary table plano_n_cancelado3 as
select  tab_aux.plano_id,
		max(marca_plano) as marca_plano
	from(
--  Cruza a tabela que foi cruzada em cima (III) com a tabela menor_parc2 p/ marcar a variavel menor_parcela nas respectivas linhas	- (IV)
	select 	tab_fp.emprestimo_id,
			tab_fp.plano_id,
			id,
			numero,
			atraso_max_dias_fpp,
			cobrado,
			juros,
			value,
			amount_paid,
			status_fpp,
			vencimento_em,
			due_date,
			date_paid,
			menor_parcela,
			case when id = installment_id then 1 else null end as marca_plano
from tab_financ_parcela3 tab_fp
join tab_installm_act2 tab_i on tab_fp.chave = tab_i.chave
join menor_parc4 mp on mp.plano_id = tab_fp.plano_id
order by tab_fp.emprestimo_id, numero, tab_fp.plano_id) as tab_aux
	group by tab_aux.plano_id;

	

/****************************************************************Query para marcar a renegociacao depois *******************************************************************/	

create temporary table marca_parcelas_qtd_min3 as
SELECT t1.emprestimo_id, parcelas_plano_min, qtd_plano, status_emespera
	from (
		select emprestimo_id, min(parcelas_qtd) as parcelas_plano_min
			from financeiro_planopagamento
			group by 1) as t1
JOIN (
	select emprestimo_id, count(id) as qtd_plano --qtd de planos por emprestimo
		from financeiro_planopagamento
		group by 1
		) as t2 on t2.emprestimo_id = t1.emprestimo_id
LEFT JOIN (
	select emprestimo_id, status as status_emespera
	from financeiro_planopagamento 
	where status = 'EmEspera') as t3 on t3.emprestimo_id = t1.emprestimo_id;




create temporary table tira_plano_null4 as
select 		tab_fp.emprestimo_id,
					tab_fp.plano_id,
					id,
					numero,
					tab_fp.chave,
					atraso_max_dias_fpp,
					parcelas_plano_min,
					qtd_plano,
					cobrado,
					juros,
					value,
					amount_paid,
					status_fpp,
					vencimento_em,
					due_date,
					date_paid,
					menor_parcela,
					marca_plano,
					status_emespera
					--aux_renegoc
from tab_financ_parcela3 tab_fp
join tab_installm_act2 tab_i on tab_fp.chave = tab_i.chave
join menor_parc4 mp on mp.plano_id = tab_fp.plano_id
join plano_n_cancelado3 pnc on pnc.plano_id = tab_fp.plano_id
join marca_parcelas_qtd_min3 mpq on mpq.emprestimo_id = tab_fp.emprestimo_id
--left join ajuste_mao am on am.emprestimo_id = tab_fp.emprestimo_id
--where tab_fp.emprestimo_id = '33'
order by tab_fp.emprestimo_id, numero, tab_fp.plano_id;


delete from tira_plano_null4 where (marca_plano is null); -- Retira os planos que tem todos status dele como "Cancelada" (marca_plano = null)



create temporary table  max_numero as
select  distinct emprestimo_id, max(numero) as max_num
from tira_plano_null4
group by 1
order by 1;

/*Marca POR EMPRESTIMO quem ja teve substituido atraves da variavel menor_parcela que foi utilizada antes para saber em qual parcela 
 * virava outro plano*/
create temporary table marca_emp as 
select *
from(
select distinct emprestimo_id,
				case when menor_parcela is not null then 1 end as marca_emprestimo
from tira_plano_null4
group by 1,2
order by 2 asc) as t1
where marca_emprestimo = 1;



create temporary table atraso16 as
select  t1.*,
		case when amount_paid is null then current_date - vencimento else date_paid - vencimento end as atraso_parcela,
		case when amount_paid is not null then 'Pago'
			 when amount_paid is null and current_date - vencimento <= 0 then 'A Vencer'
			 else 'Atrasado'
		end as status_atraso,
		case when emprestimo_id = '640' and tipo_emprestimo = 'Renegociacao' then 'Original' else tipo_emprestimo end as tipo_emprestimo2
		/*case when emprestimo_id = '535' and tipo_emprestimo = 'Antecipacaoo' then 'Original' else tipo_emprestimo end as tipo_emprestimo2   Esse eh o caso da loja de esportes que pediu para antecipar e nao pagou. Entao, como conversado com a Vanessa, 
		resolveram voltar "na mao" com as condicoes do plano original em num novo plano; Como foi o unico caso, marquei na mao	*/
from(
	select 	distinct on (chave)
					chave,
					tpn.emprestimo_id,
					plano_id,
					id,
					numero,
					atraso_max_dias_fpp,
					value,
					juros,
					amount_paid,
					status_fpp,
					date_paid,
					vencimento_em,
					due_date,
					menor_parcela,
					marca_emprestimo,
					parcelas_plano_min,
					qtd_plano,
					--aux_renegoc,
					case when numero = menor_parcela then vencimento_em else due_date end as vencimento,
					/*Quem nao aparece no campo menor_parcela eh pq n teve plano substituido ::: menor_parcela: usei para saber em qual parcela virava plano substituido :::*/
					/*ATENCAO!!! Ele nao sobrescreve, entao como o 'Pago' que se refere a ultima parcela tem prioridade, coloco na frente*/
					case when numero < menor_parcela then 'Substituido'
						 when (marca_emprestimo = 1 and numero < max_num) or (marca_emprestimo = 1 and numero = max_num and amount_paid is null) then 'Ativo'
						 when marca_emprestimo = 1 and numero = max_num and amount_paid is not null then 'Pago'
						 --when marca_emprestimo = 1 /*and numero < menor_parcela*/ then 'Ativo'
						 when marca_emprestimo is null then status_fpp
					end as status_loan,
					case when (parcelas_plano_min <> 1 and qtd_plano > 1 and status_emespera is null) or (parcelas_plano_min <> 1 and qtd_plano > 2 and status_emespera = 'EmEspera')/*and (aux_renegoc <> 'Ajuste' or aux_renegoc is null)*/ then 'Renegociacao'
						 	 --when parcelas_plano_min = 1 and  aux_renegoc = 'Subst Cancelado' then 'Antecipacao'
						 else 'Original'
					end as tipo_emprestimo,
					max_num,
					status_emespera
		from tira_plano_null4 tpn
		join max_numero mn on mn.emprestimo_id = tpn.emprestimo_id
		left join marca_emp me on me.emprestimo_id = tpn.emprestimo_id --ATENCAO!!! LEFT JOIN senao irei retirar os emprestimos que n tiveram marcados como 1 (ou seja, substituido no campo marca_emprestimo)
		--where tpn.emprestimo_id in ('640', '676', '914')
		order by chave, vencimento) as t1 /*NAO MUDAR ESSA ORDENACAO!!!*/
order by emprestimo_id, numero;



create temporary table status_loan_aux as
select  distinct on (emprestimo_id)
		emprestimo_id,
		status_loan as status_loan_aux,
		max(numero) as max_num
from atraso16
group by 1,2
order by emprestimo_id, max_num desc;



/*Status correto do status_loan*/
create temporary table status_loan_novo as
select t1.*,
		case when status_loan = 'Ativo' and status_loan_aux ='Pago' then 'Pago' else status_loan end as status_loan_atual
from (
select  a.*,
		st.status_loan_aux
from atraso16 a
join status_loan_aux st on st.emprestimo_id = a.emprestimo_id) as t1;



					
/*****************************************************************BADs*******************************************************************/
create temporary table safras_mobs20 as
select	dp.direct_prospect_id,
		a.emprestimo_id,
		a.chave,
		a.plano_id,
		a.id,
		a.numero,
		--a.atraso_max_dias_fpp,
		a.value,
		a.juros,
		a.amount_paid,
		--a.status_fpp,
		a.status_loan_atual,
		a.vencimento,
		a.date_paid,
		a.atraso_parcela,
		a.status_atraso,
		a.tipo_emprestimo2,
		lr.loan_date,
		-- lpad completa com zero; cast transforma em outro tipo de dado
		extract(year from lr.loan_date) ||'_'|| lpad(cast(extract(month from lr.loan_date) as varchar),2,'0') as cohort_loan,
		case when  a.vencimento <= (date_trunc('MONTH', loan_date) +INTERVAL '3MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '3MONTH - 1 day')::DATE end as cohort_mob2,
		case when  a.vencimento <= (date_trunc('MONTH', loan_date) +INTERVAL '4 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '4 MONTH - 1 day')::DATE end as cohort_mob3, 
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '5 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '5 MONTH - 1 day')::DATE end as cohort_mob4,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '8 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '8 MONTH - 1 day')::DATE end as cohort_mob7,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '10 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '10 MONTH - 1 day')::DATE end as cohort_mob9,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '13 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '13 MONTH - 1 day')::DATE end as cohort_mob12,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '16 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '16 MONTH - 1 day')::DATE end as cohort_mob15,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '19 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '19 MONTH - 1 day')::DATE end as cohort_mob18,
 		case when  a.vencimento <=	(date_trunc('MONTH', loan_date) + INTERVAL '25 MONTH - 1 day')::DATE then (date_trunc('MONTH', loan_date) + INTERVAL '25 MONTH - 1 day')::DATE end as cohort_mob24
from status_loan_novo a
	join loan_requests lr on lr.loan_request_id = a.emprestimo_id
	join offers o on o.offer_id = lr.offer_id
	join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
order by dp.direct_prospect_id, a.emprestimo_id, a.numero;




create temporary table max_atraso4 as
select  plano_id,
		max(case when status_atraso = 'Atrasado' then (atraso_parcela) end) as max_atraso_plano
from safras_mobs20
group by 1;



--create temporary table saldos_mobs4 as
select  direct_prospect_id,
		emprestimo_id,
		cohort_loan||''||status_loan_atual as chave_status,
		emprestimo_id||''||status_loan_atual as chave_status2,
		chave,
		plano_id,
		id,
		numero,
		value,
		juros,
		amount_paid,
		vencimento,
		date_paid,
		atraso_parcela,
		max_atraso_plano,
		--atraso_max_dias_fpp,
		status_atraso,
		status_loan_atual,
		status_loan2,
		tipo_emprestimo2,
		loan_date,
		cohort_loan,
		cohort_mob2,cohort_mob3,cohort_mob4,cohort_mob7,cohort_mob9,cohort_mob12,cohort_mob15,cohort_mob18,cohort_mob24,
		atraso_mob2,atraso_mob3,atraso_mob4,atraso_mob7,atraso_mob9,atraso_mob12,atraso_mob15,atraso_mob18,atraso_mob24,
		mob2_30,mob3_30,mob4_30,mob7_30,mob9_30,mob12_30,mob15_30,mob18_30,mob24_30,mob3_60,mob4_60,mob7_60,mob9_60,mob12_60,mob15_60,mob18_60,mob24_60,
		saldo_mob2_30,saldo_mob3_30,saldo_mob4_30,saldo_mob7_30,saldo_mob9_30,saldo_mob12_30,saldo_mob15_30,saldo_mob18_30,saldo_mob24_30,saldo_mob3_60,saldo_mob4_60,saldo_mob7_60,saldo_mob9_60,saldo_mob12_60,saldo_mob15_60,saldo_mob18_60,saldo_mob24_60,
		case when mob2_30  = 1 then numero end as parcela_mob2_30,
		case when mob3_30  = 1 then numero end as parcela_mob3_30,
		case when mob4_30  = 1 then numero end as parcela_mob4_30,
		case when mob7_30  = 1 then numero end as parcela_mob7_30,
		case when mob9_30  = 1 then numero end as parcela_mob9_30,
		case when mob12_30 = 1 then numero end as parcela_mob12_30,
		case when mob15_30 = 1 then numero end as parcela_mob15_30,
		case when mob18_30 = 1 then numero end as parcela_mob18_30,
		case when mob24_30 = 1 then numero end as parcela_mob24_30,
		--
		case when mob3_60  = 1 then numero end as parcela_mob3_60,
		case when mob4_60  = 1 then numero end as parcela_mob4_60,
		case when mob7_60  = 1 then numero end as parcela_mob7_60,
		case when mob9_60  = 1 then numero end as parcela_mob9_60,
		case when mob12_60 = 1 then numero end as parcela_mob12_60,
		case when mob15_60 = 1 then numero end as parcela_mob15_60,
		case when mob18_60 = 1 then numero end as parcela_mob18_60,
		case when mob24_60 = 1 then numero end as parcela_mob24_60
from(
	select  direct_prospect_id,
			emprestimo_id,
			chave,
			plano_id,
			id,
			numero,
			value,
			juros,
			amount_paid,
			vencimento,
			date_paid,
			atraso_parcela,
			max_atraso_plano,
			--atraso_max_dias_fpp,
			status_atraso,
			status_loan_atual,
			tipo_emprestimo2,
			case when status_loan_atual in ('Pago', 'Ativo') then 'Pago/Ativo' 
				 when status_loan_atual = 'Substituido' then 'Substituido' end as status_loan2,
			cohort_loan,
			loan_date,
			cohort_mob2,cohort_mob3,cohort_mob4,cohort_mob7,cohort_mob9,cohort_mob12,cohort_mob15,cohort_mob18,cohort_mob24,
			atraso_mob2,atraso_mob3,atraso_mob4,atraso_mob7,atraso_mob9,atraso_mob12,atraso_mob15,atraso_mob18,atraso_mob24,
			/*Marca que eh MOB se tiver atraso com mais de 30/60 dias e pega o saldo*/
			-- BAD 30 
			case when atraso_mob2  > 30 then 1 end as mob2_30,
			case when atraso_mob3  > 30 then 1 end as mob3_30,
			case when atraso_mob4> 30 then 1 end as mob4_30,
			case when atraso_mob7  > 30 then 1 end as mob7_30,
			case when atraso_mob9  > 30 then 1 end as mob9_30,
			case when atraso_mob12 > 30 then 1 end as mob12_30,
			case when atraso_mob15 > 30 then 1 end as mob15_30,
			case when atraso_mob18 > 30 then 1 end as mob18_30,
			case when atraso_mob24 > 30 then 1 end as mob24_30,
			-- BAD 30 EAD
			case when atraso_mob2 > 30 then value end as saldo_mob2_30,
			case when atraso_mob3 > 30 then value end as saldo_mob3_30,
			case when atraso_mob4  > 30 then value end as saldo_mob4_30,
			case when atraso_mob7  > 30 then value end as saldo_mob7_30,
			case when atraso_mob9  > 30 then value end as saldo_mob9_30,
			case when atraso_mob12 > 30 then value end as saldo_mob12_30,
			case when atraso_mob15 > 30 then value end as saldo_mob15_30,
			case when atraso_mob18 > 30 then value end as saldo_mob18_30,
			case when atraso_mob24 > 30 then value end as saldo_mob24_30,
			-- BAD 60
			case when atraso_mob3 > 60 then 1 end as mob3_60,
			case when atraso_mob4 > 60 then 1 end as mob4_60,
			case when atraso_mob7 > 60 then 1 end as mob7_60,
			case when atraso_mob9 > 60 then 1 end as mob9_60,
			case when atraso_mob12 > 60 then 1 end as mob12_60,
			case when atraso_mob15 > 60 then 1 end as mob15_60,
			case when atraso_mob18 > 60 then 1 end as mob18_60,
			case when atraso_mob24 > 60 then 1 end as mob24_60,
			-- BAD 60 EAD
			case when atraso_mob3  > 60 then value end as saldo_mob3_60,
			case when atraso_mob4  > 60 then value end as saldo_mob4_60,
			case when atraso_mob7  > 60 then value end as saldo_mob7_60,
			case when atraso_mob9  > 60 then value end as saldo_mob9_60,
			case when atraso_mob12 > 60 then value end as saldo_mob12_60,
			case when atraso_mob15 > 60 then value end as saldo_mob15_60,
			case when atraso_mob18 > 60 then value end as saldo_mob18_60,
			case when atraso_mob24 > 60 then value end as saldo_mob24_60
		from(
			select	direct_prospect_id,
					emprestimo_id,
					chave,
					sm.plano_id,
					id,
					numero,
					value,
					juros,
					--status_fpp,
					status_loan_atual,
					tipo_emprestimo2,
					amount_paid,
					vencimento,
					date_paid,
					atraso_parcela,
					mx.max_atraso_plano,
					--atraso_max_dias_fpp,
					status_atraso,
					cohort_loan,
					loan_date,
					cohort_mob2,cohort_mob3,cohort_mob4,cohort_mob7,cohort_mob9,cohort_mob12,cohort_mob15,cohort_mob18,cohort_mob24,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob2 then cohort_mob2 - vencimento 
							--when status_atraso = 'Pago' and date_paid > vencimento and date_paid < cohort_mob2 and cohort_mob2 < current_date then date_paid - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob2 < current_date then cohort_mob2 - vencimento	
					end as atraso_mob2,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob3 then cohort_mob3 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob3 < current_date then cohort_mob3 - vencimento	
					end as atraso_mob3,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob4 then cohort_mob4  - vencimento
						 --when status_atraso = 'Atrasado' and cohort_mob4 is not null then atraso_parcela
						 when status_atraso = 'Atrasado' and cohort_mob4 < current_date then cohort_mob4 - vencimento
					end as atraso_mob4,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob7 then cohort_mob7 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob7 < current_date then cohort_mob7 - vencimento
					end as atraso_mob7,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob9 then cohort_mob9 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob9 < current_date then cohort_mob9 - vencimento
					end as atraso_mob9,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob12 then cohort_mob12 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob12 < current_date then cohort_mob12 - vencimento
					end as atraso_mob12,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob15 then cohort_mob15 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob15 < current_date then cohort_mob15 - vencimento
					end as atraso_mob15,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob18 then cohort_mob18 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob18 < current_date then cohort_mob18 - vencimento
					end as atraso_mob18,
					case when status_atraso = 'Pago' and date_paid > vencimento and date_paid > cohort_mob24 then cohort_mob24 - vencimento 
						 when status_atraso = 'Atrasado' and cohort_mob24 < current_date then cohort_mob24 - vencimento
					end as atraso_mob24
			from safras_mobs20 sm
			join max_atraso4 mx on mx.plano_id  = sm.plano_id
			--where cohort_loan = '2017_04'
			--where emprestimo_id = '949'
			order by direct_prospect_id, emprestimo_id, numero) /*as t )*/ as t1) as t2
	--where cohort_loan between '2016_12' and '2018_03'
	group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73
	order by t2.direct_prospect_id, t2.emprestimo_id, t2.numero;