drop table zodiaco;
create TABLE zodiaco(signo char varying(12) ,dia_ini integer, mes_ini integer, dia_fim integer, mes_fim integer);
truncate table zodiaco;
select * from zodiaco;
INSERT INTO zodiaco
values
('Aries',21,3,19,4),
('Touro',20,4,20,5),
('Gemeos',21,5,20,6),
('Cancer',21,6,22,7),
('Leao',23,7,22,8),
('Virgem',23,8,22,7),
('Libra',23,9,22,10),
('Escorpiao',23,10,21,11),
('Sagitario',22,11,21,12),
('Capricornio',22,12,19,1),
('Aquario',20,1,18,2),
('Peixes',19,2,20,3);

grant all privileges on table zodiaco to root