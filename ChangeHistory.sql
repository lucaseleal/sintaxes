-- CHANGE STATUS LOAN REQUEST
SELECT a.object_pk, timestamp, changes :: JSONB -> 'status' ->> 0 as de, changes :: JSONB -> 'status' ->> 1 as para
FROM auditlog_logentry AS a
  JOIN auth_user u ON a.actor_id = u.id
WHERE content_type_id = 75
AND changes :: JSONB -> 'status' is not null
ORDER BY 2 desc;

-- CHANGE STATUS WORKFLOW (70)
SELECT a.object_pk, changes :: JSONB -> 'workflow' ->> 1 as para
FROM auditlog_logentry AS a
  JOIN auth_user u ON a.actor_id = u.id
WHERE content_type_id = 70
AND changes :: JSONB -> 'workflow' is not null and "timestamp" >= '2017-11-01' and changes :: JSONB -> 'workflow' ->> 0 = 'Analise Comite' and changes :: JSONB -> 'workflow' ->> 1 not in ('Enviar Proposta', 'Proposta Enviada') 

select cast(a.object_pk as integer) dp, changes :: JSONB -> 'workflow' ->> 1 as para
FROM auditlog_logentry AS a
JOIN auth_user u ON a.actor_id = u.id
join (SELECT a.object_pk, max("timestamp")
	FROM auditlog_logentry AS a
	  JOIN auth_user u ON a.actor_id = u.id
	WHERE content_type_id = 70 and "timestamp" >= '2017-11-01' AND changes :: JSONB -> 'workflow' is not null
	and a.object_pk in (SELECT a.object_pk FROM auditlog_logentry AS a JOIN auth_user u ON a.actor_id = u.id WHERE content_type_id = 70 and changes :: JSONB -> 'workflow' ->> 0 = 'Analise Comite')
	group by 1) as t1 on t1.object_pk = a.object_pk and t1.max = a."timestamp"
WHERE content_type_id = 70


-------------------------------------------------------CONEXÃO ENTRE BANCOS
select *
from admin_bizcap.public.auditlog_logentry lg
join app_db_bizbank.public.direct_prospects dp on lg.object_pk = dp.direct_prospect_id
limit 2

SELECT * FROM dblink('dbname=app_db_bizbank.public', 'select direct_prospect_id from direct_prospects')  AS t1(id int,code text) limit 2;

------SERASA
SELECT a.object_pk, "timestamp",changes :: JSONB -> 'serasa_4' ->> 0 as de ,changes :: JSONB -> 'serasa_4' ->> 1 as para
FROM auditlog_logentry AS a
  JOIN auth_user u ON a.actor_id = u.id
--  join (SELECT a.object_pk, max("timestamp")
--	FROM auditlog_logentry AS a
--	  JOIN auth_user u ON a.actor_id = u.id
--	WHERE content_type_id = 70 AND changes :: JSONB -> 'serasa_4' is not null
--	group by 1) as t1 on t1.object_pk = a.object_pk and t1.max = a."timestamp"
WHERE content_type_id = 70 AND changes :: JSONB -> 'serasa_4' is not null and username = 'rodrigo.pinho'

select distinct username
from auth_user

----FUNIL LOAN REQUEST = DOCUMENTO RECEBIDO
select a.object_pk::int--, changes :: JSONB -> 'status' ->> 0 as de, changes :: JSONB -> 'status' ->> 1 as para 
from auditlog_logentry a
join (select object_pk::int, max(id) id
	from auditlog_logentry
	where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'Documento Recebido'
	group by 1) as t1 on t1.id = a.id
where changes::jsonb->'status' is not null

----FUNIL LOAN REQUEST = REJECTED
select a.object_pk::int as pedido_id,
	case when a.actor_id = 25 then 'Robson' 
		when a.actor_id = 39 then 'Luigi' 
		when a.actor_id = 38 then 'Rodrigo' 
		when a.actor_id = 26 then 'Chicao' 
		when a.actor_id = 27 then 'Cristiano' 
		when a.actor_id = 55 then 'Gabriel' 
		when a.actor_id = 52 then 'Andre' 
		when a.actor_id = 40 then 'Pri' 
		when a.actor_id = 44 then 'Raphael'
		when a.actor_id = 54 then 'Angola'
		when a.actor_id = 61 then 'Bruno'
		when a.actor_id = 62 then 'Felipe'
		when a.actor_id = 64 then 'Clarissa'		
		when a.actor_id = 65 then 'Rodrigo Avila'
		when a.actor_id = 28 then 'Vanessa'
		when a.actor_id = 31 then 'Dyamilla'
		when a.actor_id = 36 then 'Giulia'
		when a.actor_id = 33 then 'Lucas'
		else a.actor_id::char(2) end as user,
	"timestamp"::date as data
from auditlog_logentry a
join (select object_pk::int, max(id) id
	from auditlog_logentry
	where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'REJECTED'
	group by 1) as t1 on t1.id = a.id
where changes::jsonb->'status' is not null

select distinct al.actor_id, au.username
from auditlog_logentry al
join auth_user au on au.id = al.actor_id


----FUNIL LOAN REQUEST = DOCUMENTO RECEBIDO
select a.object_pk || ',',changes :: JSONB -> 'status' ->> 0,changes :: JSONB -> 'status' ->> 1
from auditlog_logentry a
join (select object_pk::int, max(id) id
	from auditlog_logentry
	where content_type_id = 75 and changes :: JSONB -> 'status' ->> 0 = 'ACCEPTED'
	group by 1) as t1 on t1.id = a.id
where changes::jsonb->'status' is not null and to_char("timestamp",'Mon/yy') = 'Sep/18'

----FUNIL LOAN REQUEST = ALL DOCs
select distinct '(' || object_id || ')' || ','
from auditlog_logentry
where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'Alçada 1'

----FUNIL LEAD STATUS AUTOMATICO OU NAO
select a.object_pk,changes :: JSONB -> 'workflow' ->> 1
from auditlog_logentry a
join (select object_pk::int, max(id) id
	from auditlog_logentry
	where content_type_id = 70 and changes :: JSONB -> 'workflow'  is not null
	group by 1) as t1 on t1.id = a.id

select max(object_id)
from auditlog_logentry
where content_type_id = 70 and changes :: JSONB -> 'workflow' ->> 0 not in ('Enviar Proposta','Proposta Enviada') and changes :: JSONB -> 'workflow' ->> 1 not in ('Enviar Proposta','Proposta Enviada') 
	and changes :: JSONB -> 'workflow' is not null and object_id < 50000 and changes :: JSONB -> 'workflow' ->> 1 <> 'Nao Processado' and changes :: JSONB -> 'workflow' ->> 1 <> '' and 
		changes :: JSONB -> 'workflow' ->> 0 <> 'Analise Comite'

select changes :: JSONB -> 'workflow',"timestamp"
from auditlog_logentry
where content_type_id = 70 and object_pk::int = 3769

select a.object_id as id,changes :: JSONB -> 'workflow' ->> 1 as status_final
from auditlog_logentry a
join (select object_pk::int, max(id) id
	from auditlog_logentry
	where content_type_id = 70 and changes :: JSONB -> 'workflow'  is not null
	group by 1) as t1 on t1.id = a.id

select distinct object_id
from auditlog_logentry
where content_type_id = 70 and changes :: JSONB -> 'workflow'  is not null
order by 1
limit 500

select object_id, changes :: JSONB -> 'status',"timestamp"
from auditlog_logentry
where content_type_id = 75 and changes :: JSONB -> 'status' is not null

select object_id, max("timestamp")
from auditlog_logentry
where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'Investigação Crédito' and actor_id = 38
group by 1

select a.object_id,max,case actor_id when 61 then 'Bruno' when 62 then 'Felipe' when 65 then 'Rodrigo' else 'Outro' end as "Analista de doc"
from auditlog_logentry a
join(select object_id, max("timestamp")
	from auditlog_logentry
	where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'Alçada 1' and actor_id in (61,62,65)
	group by 1) as t1 on t1.object_id = a.object_id and t1.max = a."timestamp"

select to_date(to_char(time,'Mon/yy'),'Mon/yy') as safra,
	count(*) as alldoc
from(select object_id, max("timestamp")::date as time from auditlog_logentry where content_type_id = 75 and changes :: JSONB -> 'status' ->> 1 = 'Alçada 1' group by 1) as t1
group by 1

--VALOR SOLICITADO
select *--object_id as loan_request_id, -- , (changes::jsonb->'value'->> 1)::float as valor_solicitado,(changes::jsonb->'number_of_installments'->> 1)::float as prazo_solicitado
from auditlog_logentry a
where content_type_id = 75 and object_id = 13579 --and changes::jsonb->'value'->> 0 = 'None' 

select a.object_id,left(a.object_repr,position('k' in a.object_repr)-1)::float * 1000 as valor_solicitado,replace(substring(a.object_repr,position('k em ' in a.object_repr)+5,2),'x','')::float as prazo_solicitado
from auditlog_logentry a
join(select object_id,min(id) as min_id
	from auditlog_logentry
	where content_type_id = 75
	group by 1) as t1 on t1.object_id = a.object_id and a.id = t1.min_id
	
--PEDIDO FOI ASSINADO POR ANALISTA DE is
select coalesce(t1.object_id,t2.object_id) as object_id,coalesce(t1.first_name,t2.first_name) as first_name
from(select a.object_id,u.first_name
	from auditlog_logentry a
	join(select object_id,min(id) min_id
		from auditlog_logentry
		where content_type_id = 75 and (actor_id in (40,64,76,70,54,42,60,64,44,31) or (actor_id = 65 and "timestamp" < '2018-11-12'))
		group by 1) as t1 on t1.min_id = a.id
	join auth_user u on u.id = a.actor_id) as t1
full outer join(select t1.object_id,u.first_name
	from django_admin_log dl
	join(select object_id::int,min(id) min_id
		from django_admin_log
		where content_type_id = 75 and change_message like '%como analista%' and (user_id in (40,64,76,70,54,42,60,64,44,31) or (user_id = 65 and action_time < '2018-11-12'))
		group by 1) as t1 on t1.min_id = dl.id
	join auth_user u on u.id = dl.user_id) as t2 on t2.object_id = t1.object_id
