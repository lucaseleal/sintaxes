--CONSERTO DE INCOSITÊCIAS NO WORKFLOW DE LEADS, OFERTAS, PEDIDOS
update offers
set status = 'Substituida'
where offer_id in (select o.offer_id
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	left join loan_requests lr on lr.offer_id = o.offer_id
	where workflow like 'PedidoRejeitado' and o.status like 'PedidoPerdido')

--SUBSTITUIÇÃO DA DATA COLETA PELO LOAN DATE PARA OS PEDIDOS QUE TIVERAM SCR TIRADOS RETROATIVAMENTE (back up: data_coleta antiga = data_criada
update credito_coleta cc
set data_coleta = lr.loan_date::timestamp
from offers o, loan_requests lr
where o.direct_prospect_id = cc.origem_id and lr.offer_id = o.offer_id and lr.status = 'ACCEPTED' and id in (
	select cc.id
	from credito_coleta cc
	join offers o on o.direct_prospect_id = cc.origem_id
	join loan_requests lr on lr.offer_id = o.offer_id
	where lr.status = 'ACCEPTED' and data_coleta::date = '2018-06-22' and origem_id <= 7068)

--SETA SITE PRA true CONSIDERANDO OS QUE TEM CONTACT PAGE
update direct_prospects
set site = true
where direct_prospect_id in (select dp.direct_prospect_id
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	where lr.status = 'ACCEPTED' and dp.site is null and upper((case when dp.contact_page = '' then null else dp.contact_page end)) like '%WWW%' and upper((case when dp.contact_page = '' then null else dp.contact_page end)) not like '%FACEBOOK%'
	and dp.direct_prospect_id not in (1400,22408,43841,21185,1040,25381,12699,22557,15758,12338,1106,66594,24263))

--CAMPO FACEBOOK
update direct_prospects
set site = true
where direct_prospect_id in (33534,
13144,
13307,
6483,
24811,
24730,
2698,
43327,
58816,
1576,
2137,
64368,
62263,
70503,
54906,
68831,
82156,
54274,
8083,
58181,
45139,
34637,
63647,
43766,
45658,
1404,
1085,
66994,
20431)